TODO List
===

We have moved the Project Management System to Asana.

Please visit: https://asana.com

DB related
===

phpmyadmin: http://avh.luxing.im/phpmyadmin

credentials:

phpbb/xfePeVXPuYR42W43

dp/Amejqpf5adHDX8rJ

Artifacts
===

Docs: http://file.luxing.im

Actual websites: https://avh.luxing.im

Server IP: 192.99.186.133

Iteration 2
===

 1. Tracking
 2. Traffic Analyzing

Tracking related
===

Piwik: https://avh.luxing.im/piwik

Apply users via Luxing

NOTES
===

When drupal upgrade, always comment out the line #273 in dp/includes/session.inc

```php
date_default_timezone_set(drupal_get_user_timezone());
```

Otherwise it will generate a timezone error whenever a user logs in.

When port to other machines, change the configuration files:

  1. /var/www/bb/config.php
  2. /var/www/dp/sites/default/settings.php
  3. /var/www/piwik/config/*
